-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

Format: 3.0 (native)
Source: steam
Binary: steam-launcher, steam, steam-libs-amd64, steam-libs-i386
Architecture: all i386 amd64
Version: 1:1.0.0.76
Maintainer: Valve Corporation <linux@steampowered.com>
Homepage: http://www.steampowered.com/
Standards-Version: 3.9.4
Build-Depends: debhelper (>= 8.1.0), dh-python | python3 (<< 3.3.2-10~), perl, python3
Package-List: 
 steam deb oldlibs extra
 steam-launcher deb games optional
 steam-libs-amd64 deb games optional
 steam-libs-i386 deb games optional
Checksums-Sha1: 
 2a621a839ec5d7a72dc3ab158b8d1cd8dd7b78e6 3782461 steam_1.0.0.76.tar.gz
Checksums-Sha256: 
 5fb8b104ba1434b94c6e44373edfbb6188c8ef305f02bcd519b809f5e28f23fd 3782461 steam_1.0.0.76.tar.gz
Files: 
 9db32c129bac8a40080d97888f028c3b 3782461 steam_1.0.0.76.tar.gz

-----BEGIN PGP SIGNATURE-----

iQFLBAEBCgA1FiEEuhgW7451AF/PXieh8krqn7BUmLcFAmP/oQ8XHGxpbnV4QHN0
ZWFtcG93ZXJlZC5jb20ACgkQ8krqn7BUmLdjNgf/XZshYyWMd7gnoUNIIx8DSlGP
vm/9bQAOIzChjmfPI5zTAgC/lKciruiBgf8QJWeEUZTnW32xHpZclLWMPQTCPEc8
e5j1FXqcOXE7MATzGTTu7H0cggbVFFyQslgMCMlWWAiiS2gXfQIbDPgcOtRr/gjM
DYUpd3AtTrO71/EaJZSBM3TR+aQ8+2DYKUsN7g/0594xo/fYtfIb0pLtgVW9+/fs
k93MXNnYYDaLVgZrM0zGQzSsbEQWBiOg9yigsi+QQHcdfFo5yQTRf87YOqOd8KZl
JnYJP5zload7O/iAujK5ttGoluYx/xOA3rMprd3lXS0a0JkJgS7GUI5PtJsT6w==
=ZST3
-----END PGP SIGNATURE-----
